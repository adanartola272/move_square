extern crate piston_window;
extern crate rand;

use piston_window::*;
use rand::Rng;

struct Coord (f64,f64,f64,f64);
enum Color {
    Red, Green, Blue, Magenta, Cyan, Yellow, Orange,
}

struct Square{
    x:f64,
    y:f64,
    vx: f64,
    vy: f64,
    size: f64,   
}

impl Square{

    fn random_position(&mut self,windowSize:f64){
        self.x =self.randCoord(windowSize);
        self.y =self.randCoord(windowSize);
    }

    fn on_press(&mut self, windowSize:f64, args: &Button) {
        
        match args {
            Button::Keyboard(key) => { self.on_key(windowSize,key); }
            _ => {},
        }
    }

    fn on_key(&mut self,windowSize:f64,key: &Key) {
            
        let movement = match key {
            Key::Right => Some((1.0, 0.0)),
            Key::Left  => Some((-1.0, 0.0)),
            Key::Down  => Some((0.0, 1.0)),
            Key::Up    => Some((0.0, -1.0)),
            _ => None,
        };

        if let Some(movement) = movement {

            let (dx,dy) = movement;
            if self.x + dx*self.vx>0.0 && 
               self.x + dx*self.vx + self.size <windowSize{  
                
               self.x = self.x + dx*self.vx;
            }
            
            if self.y + dy*self.vy>0.0 && 
               self.y + dy*self.vy + self.size  <windowSize{    
               
               self.y = self.y + dy*self.vy;
            }
        }
    }

    fn randCoord(&self,windowSize:f64)->f64{
        rand::thread_rng().gen_range(1.0, windowSize-self.size+1.0) 
    }
}

fn main() {

    let windowSize = 600.0;
    let squareSideLenght = 50.0;
    let mut window: PistonWindow = WindowSettings::new(
            "piston: hello_world",
            [windowSize, windowSize]
        )
        .exit_on_esc(true)
        //.opengl(OpenGL::V2_1) // Set a different OpenGl version
        .build()
        .unwrap();

   let mut mySquare = Square{
       x: 1.0,
       y:  1.0,
       vx: 10.0,
       vy: 10.0,
       size: 50.0, 
   };

   mySquare.random_position(windowSize);
    
    //window.set_lazy(true);
    
    while let Some(e) = window.next() {
       
        //change_coords(squareSideLenght, windowSize,&mut coord);
        
        if let Some(args) = e.press_args() {
            mySquare.on_press(windowSize,&args);
        }

        window.draw_2d(&e, |c, g, device| {
            
            let transform = c.transform;

            clear([0.0, 0.0, 0.0, 1.0], g); 
            rectangle([0.0, 1.0, 1.0, 1.0], rectangle::square(mySquare.x,mySquare.y,mySquare.size), transform, g); 
        });
    }
}


